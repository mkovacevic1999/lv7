﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iks_oks
{
    public partial class Form1 : Form
    {

        Circle c = new Circle();
        Cross cross = new Cross();

        public static Panel panel1 = new Panel();
        bool[] jeCrtano = new bool[9]; // namjesteno po defaultu an false
        Graphics g; // za fence na paneli 
        Graphics g1; // za svaki picturebox
        Graphics g2;
        Graphics g3;
        Graphics g4;
        Graphics g5;
        Graphics g6;
        Graphics g7;
        Graphics g8;
        Graphics g9;
        Form2 secondForm = new Form2();
        public static Label lb1 = new Label();
        public static Label lb2 = new Label();
        Pen Penzacrtanje = new Pen(Color.Black, 8);
        int op1 = 0;
        int op2 = 0;
        int op3 = 0;
        int op4 = 0;
        int op5 = 0;
        int op6 = 0;
        int op7 = 0;
        int op8 = 0;
        int op9 = 0;
        int rezultatPrvi = 0;
        int rezultatDrugi = 0;
        public Form1()
        {
            InitializeComponent();
            g = pnl1.CreateGraphics();
            g1 = pb1.CreateGraphics();
            g2 = pb2.CreateGraphics();
            g3 = pb3.CreateGraphics();
            g4 = pb4.CreateGraphics();
            g5 = pb5.CreateGraphics();
            g6 = pb6.CreateGraphics();
            g7 = pb7.CreateGraphics();
            g8 = pb8.CreateGraphics();
            g9 = pb9.CreateGraphics();
        

        }

        private void btnIgraj_Click(object sender, EventArgs e)
        {
           
            
            if (secondForm.ShowDialog() == DialogResult.OK)
            {
                btnIgraj.Enabled = false;
                lblNaPotezu.Text = lblPrvi.Text;
                pb1.Show();
                pb2.Show();
                pb3.Show();
                pb4.Show();
                pb5.Show();
                pb6.Show();
                pb7.Show();
                pb8.Show();
                pb9.Show();
                btnZavrši.Enabled = true;
            }            else
            {
                btnIgraj.Enabled = true;
                
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lb1 = lblPrvi;
            lb2 = lblDrugi;
            lblrez1.Text = "0";
            lblrez2.Text = "0";
              
            op1 = 0;
            op2 = 0;
            op3 = 0;
            op4 = 0;
            op5 = 0;
            op6 = 0;
            op7 = 0;
            op8 = 0;
            op9 = 0;
            pb1.Hide();
            pb2.Hide();
            pb3.Hide();
            pb4.Hide();
            pb5.Hide();
            pb6.Hide();
            pb7.Hide();
            pb8.Hide();
            pb9.Hide();
            btnZavrši.Enabled = false;
         

        }

        private void pnl1_Paint(object sender, PaintEventArgs e)
        {
            
            panel1 = pnl1;
            Fence  engine = new Fence();
            engine.SetupCanvas(g);
        }

        class Fence
        {
            
            public void SetupCanvas(Graphics g)
            {
                Pen lines = new Pen(Color.Black);
               
                g.DrawLine(lines, new Point(panel1.Width / 3, 0), new Point(panel1.Width / 3, (panel1.Height / 10) * 8));
                g.DrawLine(lines, new Point((panel1.Width / 3) * 2, 0), new Point((panel1.Width / 3) * 2, (panel1.Height / 10) * 8));
            
                g.DrawLine(lines, new Point(0, (panel1.Height / 10 * 8) / 3), new Point(panel1.Width, (panel1.Height / 10 * 8) / 3));
                g.DrawLine(lines, new Point(0, ((panel1.Height / 10 * 8) / 3) * 2), new Point(panel1.Width, ((panel1.Height / 10 * 8) / 3) * 2));



            }
        }

  

        private void lblrez1_Click(object sender, EventArgs e)
        {

        }

        private void lblPrvi_Click(object sender, EventArgs e)
        {

        }

        private void pb1_Click(object sender, EventArgs e)
        {  
            if (jeCrtano[0] == false)
            {
                jeCrtano[0] = true;
                if (lblNaPotezu.Text == lblPrvi.Text)
                {
                    cross.draw(g1, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);                
                    op1 = 1;
                    lblNaPotezu.Text = lblDrugi.Text;
                    provjera1(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                
                }

                else
                {
                    c.draw(g1, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);   
                    op1 = 2;
                    lblNaPotezu.Text = lblPrvi.Text;
                    provjera2(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                 
                }
            }

            else 
            {
                MessageBox.Show("Vec je popunjeno ovo polje", "Greska");
            }
            
         
        }
        private void provjera1(int op1, int op2, int op3, int op4, int op5, int op6, int op7, int op8, int op9)
        {
            if ((op1 == 1 && op4 == 1 && op7 == 1) || (op1 == 1 && op2 == 1 && op3 == 1) || (op3 == 1 && op6 == 1 && op9 == 1) || (op7 == 1 && op8 == 1 && op9 == 1) || (op2 == 1 && op5 == 1 && op8 == 1) || (op1 == 1 && op5 == 1 && op9 == 1) || (op3 == 1 && op5 == 1 && op7 == 1) || (op4 == 1 && op5 == 1 && op6 == 1))
            {

                
                rezultatPrvi = Convert.ToInt32(lblrez1.Text);
                rezultatPrvi++;
                lblrez1.Text = rezultatPrvi.ToString();
                MessageBox.Show("Pobjednik partije je " + lblPrvi.Text + " i dobiva 1 bod na svoj rezultat", "Rezultat");
                novapartija();
            }
            else if (provjeranerijesenosti(jeCrtano))
            {
                MessageBox.Show("Nerijeseno", "Rezultat");
                novapartija();
            }

        }
        private void provjera2 (int op1, int op2, int op3, int op4, int op5, int op6, int op7, int op8, int op9)
        {
            if ((op1 == 2 && op4 == 2 && op7 == 2) || (op1 == 2 && op2 == 2 && op3 == 2) || (op3 == 2 && op6 == 2 && op9 == 2) || (op7 == 2 && op8 == 2 && op9 == 2) || (op2 == 2 && op5 == 2 && op8 == 2) || (op1 == 2 && op5 == 2 && op9 == 2) || (op3 == 2 && op5 == 2 && op7 == 2) || (op4 == 2 && op5 == 2 && op6 == 2))
            {

                
                rezultatDrugi = Convert.ToInt32(lblrez2.Text);
                rezultatDrugi++;
                lblrez2.Text = rezultatDrugi.ToString();
                MessageBox.Show("Pobjednik partije je " + lblDrugi.Text + " i dobiva 1 bod na svoj rezultat", "Rezultat");
                novapartija();
            }
            else if (provjeranerijesenosti(jeCrtano))
            {
                
                MessageBox.Show("Nerijeseno", "Rezultat");
                novapartija();
            }

        }

        private bool provjeranerijesenosti(bool [] jeCrtano)
        {
            int brojac = 0;
            for (int i = 0; i<9; i++)
            {
                if (jeCrtano[i] == true)
                    brojac++;                 
            }
            if (brojac == 9)
                return true;
            else
                return false;

        }

        private void novapartija()
        {
            pb1.Image = null;
            pb2.Image = null;
            pb3.Image = null;
            pb4.Image = null;
            pb5.Image = null;
            pb6.Image = null;
            pb7.Image = null;
            pb8.Image = null;
            pb9.Image = null;
            op1 = 0;
            op2 = 0;
            op3 = 0;
            op4 = 0;
            op5 = 0;
            op6 = 0;
            op7 = 0;
            op8 = 0;
            op9 = 0;
            for (int i = 0; i < 9; i++)
            {
                jeCrtano[i] = false;
            }
        }
        private void pb1_MouseUp(object sender, MouseEventArgs e)
        {
          


        }

        class Circle : IDrawable
        {
            int r;
            public Circle()
            {
                r = 60;
            }
            public void draw(Graphics g, Pen p, int x, int y)
            {
                g.DrawEllipse(p, x, y, r, r);
            }
        }

        class Cross : IDrawable
        {

            public void draw(Graphics g, Pen p, int x, int y)
            {
                Point[] points = new Point[4]{ new Point(x,y), new Point(x + 60,y+60),
                new Point(x + 60, y), new Point(x, y+60)};
                g.DrawLine(p, points[0],points[1]);
                g.DrawLine(p, points[2], points[3]);
            }
        }

        interface IDrawable
        {
            void draw(Graphics g, Pen p, int x, int y);
        }

        private void pb2_Click(object sender, EventArgs e)
        {
            if (jeCrtano[1] == false)
            {
                jeCrtano[1] = true;
                if (lblNaPotezu.Text == lblPrvi.Text)
                {
                    cross.draw(g2, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblDrugi.Text;
                    op2 = 1;
                    provjera1(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }

                else
                {
                    c.draw(g2, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblPrvi.Text;
                    op2 = 2;
                    provjera2(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }
            }

            else
            {
                MessageBox.Show("Vec je popunjeno ovo polje", "Greska");
            }

        }

        private void pb3_Click(object sender, EventArgs e)
        {
            if (jeCrtano[2] == false)
            {
                jeCrtano[2] = true;
                if (lblNaPotezu.Text == lblPrvi.Text)
                {
                    cross.draw(g3, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblDrugi.Text;
                    op3 = 1;
                    provjera1(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }

                else
                {
                    c.draw(g3, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblPrvi.Text;
                    op3 = 2;
                    provjera2(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }
            }

            else
            {
                MessageBox.Show("Vec je popunjeno ovo polje", "Greska");
            }

        }

        private void pb4_Click(object sender, EventArgs e)
        {
            if (jeCrtano[3] == false)
            {
                jeCrtano[3] = true;
                if (lblNaPotezu.Text == lblPrvi.Text)
                {
                    cross.draw(g4, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblDrugi.Text;
                    op4 = 1;
                    provjera1(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }

                else
                {
                    c.draw(g4, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblPrvi.Text;
                    op4 = 2;
                    provjera2(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }
            }

            else
            {
                MessageBox.Show("Vec je popunjeno ovo polje", "Greska");
            }

        }

        private void pb5_Click(object sender, EventArgs e)
        {
            if (jeCrtano[4] == false)
            {
                jeCrtano[4] = true;
                if (lblNaPotezu.Text == lblPrvi.Text)
                {
                    cross.draw(g5, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblDrugi.Text;
                    op5 = 1;
                    provjera1(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }

                else
                {
                    c.draw(g5, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblPrvi.Text;
                    op5 = 2;
                    provjera2(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }
            }

            else
            {
                MessageBox.Show("Vec je popunjeno ovo polje", "Greska");
            }

        }

        private void pb6_Click(object sender, EventArgs e)
        {
            if (jeCrtano[5] == false)
            {
                jeCrtano[5] = true;
                if (lblNaPotezu.Text == lblPrvi.Text)
                {
                    cross.draw(g6, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblDrugi.Text;
                    op6 = 1;
                    provjera1(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }

                else
                {
                    c.draw(g6, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblPrvi.Text;
                    op6 = 2;
                    provjera2(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }
            }

            else
            {
                MessageBox.Show("Vec je popunjeno ovo polje", "Greska");
            }

        }

        private void pb7_Click(object sender, EventArgs e)
        {
            if (jeCrtano[6] == false)
            {
                jeCrtano[6] = true;
                if (lblNaPotezu.Text == lblPrvi.Text)
                {
                    cross.draw(g7, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblDrugi.Text;
                    op7 = 1;
                    provjera1(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }

                else
                {
                    c.draw(g7, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblPrvi.Text;
                    op7 = 2;
                    provjera2(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }
            }

            else
            {
                MessageBox.Show("Vec je popunjeno ovo polje", "Greska");
            }

        }

        private void pb8_Click(object sender, EventArgs e)
        {
            if (jeCrtano[7] == false)
            {
                jeCrtano[7] = true;
                if (lblNaPotezu.Text == lblPrvi.Text)
                {
                    cross.draw(g8, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblDrugi.Text;
                    op8 = 1;
                    provjera1(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }

                else
                {
                    c.draw(g8, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblPrvi.Text;
                    op8 = 2;
                    provjera2(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }
            }

            else
            {
                MessageBox.Show("Vec je popunjeno ovo polje", "Greska");
            }

        }

        private void pb9_Click(object sender, EventArgs e)
        {
            if (jeCrtano[8] == false)
            {
                jeCrtano[8] = true;
                if (lblNaPotezu.Text == lblPrvi.Text)
                {
                    cross.draw(g9, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblDrugi.Text;
                    op9 = 1;
                    provjera1(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }

                else
                {
                    c.draw(g9, Penzacrtanje, pb1.Width / 2 - 30, pb1.Height / 2 - 30);
                    lblNaPotezu.Text = lblPrvi.Text;
                    op9 = 2;
                    provjera2(op1, op2, op3, op4, op5, op6, op7, op8, op9);
                }
            }

            else
            {
                MessageBox.Show("Vec je popunjeno ovo polje", "Greska");
            }

        }

        private void btnZavrši_Click(object sender, EventArgs e)
        {
            int rez1;
            int rez2;
            rez1 = Convert.ToInt32(lblrez1.Text);
            rez2 = Convert.ToInt32(lblrez2.Text);
            if (rez1 > rez2)
            {
                MessageBox.Show("Pobjednik igre je " + lblPrvi.Text  + ", gasim igru!", "Pobjednik");
                Application.Exit();
            }

            else if (rez1 < rez2)
            {
                MessageBox.Show("Pobjednik igre je " + lblDrugi.Text + ", gasim igru!", "Pobjednik");
                Application.Exit();
            }

            else
            {
                MessageBox.Show("Igra je završila neriješeno, gasim igru!", "Pobjednik");
                Application.Exit();
            }
        }
    }
}
