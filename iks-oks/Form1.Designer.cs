﻿namespace iks_oks
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnIgraj = new System.Windows.Forms.Button();
            this.lblPrvi = new System.Windows.Forms.Label();
            this.lblDrugi = new System.Windows.Forms.Label();
            this.lblrez1 = new System.Windows.Forms.Label();
            this.lblrez2 = new System.Windows.Forms.Label();
            this.pnl1 = new System.Windows.Forms.Panel();
            this.pb9 = new System.Windows.Forms.PictureBox();
            this.pb8 = new System.Windows.Forms.PictureBox();
            this.pb7 = new System.Windows.Forms.PictureBox();
            this.pb6 = new System.Windows.Forms.PictureBox();
            this.pb5 = new System.Windows.Forms.PictureBox();
            this.pb4 = new System.Windows.Forms.PictureBox();
            this.pb3 = new System.Windows.Forms.PictureBox();
            this.pb2 = new System.Windows.Forms.PictureBox();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNaPotezu = new System.Windows.Forms.Label();
            this.btnZavrši = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnIgraj
            // 
            this.btnIgraj.Location = new System.Drawing.Point(613, 213);
            this.btnIgraj.Name = "btnIgraj";
            this.btnIgraj.Size = new System.Drawing.Size(151, 68);
            this.btnIgraj.TabIndex = 0;
            this.btnIgraj.Text = "Igraj!";
            this.btnIgraj.UseVisualStyleBackColor = true;
            this.btnIgraj.Click += new System.EventHandler(this.btnIgraj_Click);
            // 
            // lblPrvi
            // 
            this.lblPrvi.AutoSize = true;
            this.lblPrvi.Location = new System.Drawing.Point(610, 79);
            this.lblPrvi.Name = "lblPrvi";
            this.lblPrvi.Size = new System.Drawing.Size(43, 13);
            this.lblPrvi.TabIndex = 1;
            this.lblPrvi.Text = "Igrač 1:";
            this.lblPrvi.Click += new System.EventHandler(this.lblPrvi_Click);
            // 
            // lblDrugi
            // 
            this.lblDrugi.AutoSize = true;
            this.lblDrugi.Location = new System.Drawing.Point(610, 123);
            this.lblDrugi.Name = "lblDrugi";
            this.lblDrugi.Size = new System.Drawing.Size(43, 13);
            this.lblDrugi.TabIndex = 2;
            this.lblDrugi.Text = "Igrač 2:";
            // 
            // lblrez1
            // 
            this.lblrez1.AutoSize = true;
            this.lblrez1.Location = new System.Drawing.Point(706, 79);
            this.lblrez1.Name = "lblrez1";
            this.lblrez1.Size = new System.Drawing.Size(35, 13);
            this.lblrez1.TabIndex = 3;
            this.lblrez1.Text = "label1";
            this.lblrez1.Click += new System.EventHandler(this.lblrez1_Click);
            // 
            // lblrez2
            // 
            this.lblrez2.AutoSize = true;
            this.lblrez2.Location = new System.Drawing.Point(706, 123);
            this.lblrez2.Name = "lblrez2";
            this.lblrez2.Size = new System.Drawing.Size(35, 13);
            this.lblrez2.TabIndex = 4;
            this.lblrez2.Text = "label2";
            // 
            // pnl1
            // 
            this.pnl1.Controls.Add(this.pb9);
            this.pnl1.Controls.Add(this.pb8);
            this.pnl1.Controls.Add(this.pb7);
            this.pnl1.Controls.Add(this.pb6);
            this.pnl1.Controls.Add(this.pb5);
            this.pnl1.Controls.Add(this.pb4);
            this.pnl1.Controls.Add(this.pb3);
            this.pnl1.Controls.Add(this.pb2);
            this.pnl1.Controls.Add(this.pb1);
            this.pnl1.Location = new System.Drawing.Point(29, 27);
            this.pnl1.Name = "pnl1";
            this.pnl1.Size = new System.Drawing.Size(492, 371);
            this.pnl1.TabIndex = 5;
            this.pnl1.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl1_Paint);
            // 
            // pb9
            // 
            this.pb9.BackColor = System.Drawing.SystemColors.Control;
            this.pb9.Location = new System.Drawing.Point(330, 202);
            this.pb9.Name = "pb9";
            this.pb9.Size = new System.Drawing.Size(159, 95);
            this.pb9.TabIndex = 8;
            this.pb9.TabStop = false;
            this.pb9.Click += new System.EventHandler(this.pb9_Click);
            // 
            // pb8
            // 
            this.pb8.BackColor = System.Drawing.SystemColors.Control;
            this.pb8.Location = new System.Drawing.Point(165, 202);
            this.pb8.Name = "pb8";
            this.pb8.Size = new System.Drawing.Size(159, 95);
            this.pb8.TabIndex = 7;
            this.pb8.TabStop = false;
            this.pb8.Click += new System.EventHandler(this.pb8_Click);
            // 
            // pb7
            // 
            this.pb7.BackColor = System.Drawing.SystemColors.Control;
            this.pb7.Location = new System.Drawing.Point(0, 202);
            this.pb7.Name = "pb7";
            this.pb7.Size = new System.Drawing.Size(159, 95);
            this.pb7.TabIndex = 6;
            this.pb7.TabStop = false;
            this.pb7.Click += new System.EventHandler(this.pb7_Click);
            // 
            // pb6
            // 
            this.pb6.BackColor = System.Drawing.SystemColors.Control;
            this.pb6.Location = new System.Drawing.Point(330, 101);
            this.pb6.Name = "pb6";
            this.pb6.Size = new System.Drawing.Size(159, 95);
            this.pb6.TabIndex = 5;
            this.pb6.TabStop = false;
            this.pb6.Click += new System.EventHandler(this.pb6_Click);
            // 
            // pb5
            // 
            this.pb5.BackColor = System.Drawing.SystemColors.Control;
            this.pb5.Location = new System.Drawing.Point(165, 101);
            this.pb5.Name = "pb5";
            this.pb5.Size = new System.Drawing.Size(159, 95);
            this.pb5.TabIndex = 4;
            this.pb5.TabStop = false;
            this.pb5.Click += new System.EventHandler(this.pb5_Click);
            // 
            // pb4
            // 
            this.pb4.BackColor = System.Drawing.SystemColors.Control;
            this.pb4.Location = new System.Drawing.Point(0, 101);
            this.pb4.Name = "pb4";
            this.pb4.Size = new System.Drawing.Size(159, 95);
            this.pb4.TabIndex = 3;
            this.pb4.TabStop = false;
            this.pb4.Click += new System.EventHandler(this.pb4_Click);
            // 
            // pb3
            // 
            this.pb3.BackColor = System.Drawing.SystemColors.Control;
            this.pb3.Location = new System.Drawing.Point(330, 0);
            this.pb3.Name = "pb3";
            this.pb3.Size = new System.Drawing.Size(159, 95);
            this.pb3.TabIndex = 2;
            this.pb3.TabStop = false;
            this.pb3.Click += new System.EventHandler(this.pb3_Click);
            // 
            // pb2
            // 
            this.pb2.BackColor = System.Drawing.SystemColors.Control;
            this.pb2.Location = new System.Drawing.Point(165, 0);
            this.pb2.Name = "pb2";
            this.pb2.Size = new System.Drawing.Size(159, 95);
            this.pb2.TabIndex = 1;
            this.pb2.TabStop = false;
            this.pb2.Click += new System.EventHandler(this.pb2_Click);
            // 
            // pb1
            // 
            this.pb1.BackColor = System.Drawing.SystemColors.Control;
            this.pb1.Location = new System.Drawing.Point(0, 0);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(159, 95);
            this.pb1.TabIndex = 0;
            this.pb1.TabStop = false;
            this.pb1.Click += new System.EventHandler(this.pb1_Click);
            this.pb1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb1_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(116, 415);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "NA POTEZU:";
            // 
            // lblNaPotezu
            // 
            this.lblNaPotezu.AutoSize = true;
            this.lblNaPotezu.Location = new System.Drawing.Point(210, 415);
            this.lblNaPotezu.Name = "lblNaPotezu";
            this.lblNaPotezu.Size = new System.Drawing.Size(0, 13);
            this.lblNaPotezu.TabIndex = 7;
            // 
            // btnZavrši
            // 
            this.btnZavrši.Location = new System.Drawing.Point(613, 302);
            this.btnZavrši.Name = "btnZavrši";
            this.btnZavrši.Size = new System.Drawing.Size(151, 67);
            this.btnZavrši.TabIndex = 8;
            this.btnZavrši.Text = "Završi igru";
            this.btnZavrši.UseVisualStyleBackColor = true;
            this.btnZavrši.Click += new System.EventHandler(this.btnZavrši_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(757, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "X";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(757, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "O";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnZavrši);
            this.Controls.Add(this.lblNaPotezu);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnl1);
            this.Controls.Add(this.lblrez2);
            this.Controls.Add(this.lblrez1);
            this.Controls.Add(this.lblDrugi);
            this.Controls.Add(this.lblPrvi);
            this.Controls.Add(this.btnIgraj);
            this.Name = "Form1";
            this.Text = "iks-oks";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIgraj;
        private System.Windows.Forms.Label lblPrvi;
        private System.Windows.Forms.Label lblDrugi;
        private System.Windows.Forms.Label lblrez1;
        private System.Windows.Forms.Label lblrez2;
        private System.Windows.Forms.Panel pnl1;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.PictureBox pb3;
        private System.Windows.Forms.PictureBox pb2;
        private System.Windows.Forms.PictureBox pb9;
        private System.Windows.Forms.PictureBox pb8;
        private System.Windows.Forms.PictureBox pb7;
        private System.Windows.Forms.PictureBox pb6;
        private System.Windows.Forms.PictureBox pb5;
        private System.Windows.Forms.PictureBox pb4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNaPotezu;
        private System.Windows.Forms.Button btnZavrši;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

